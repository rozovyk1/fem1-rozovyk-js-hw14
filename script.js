/*
Задание

Переделать домашнее задание 9 (табы), используя jQuery
*/

$(".tabs li").click(function () {
    $(this).addClass('active-tab').parents('ul.tabs').find('li').not($(this)).removeClass('active-tab');
    const currentTabIndex = $(this).index();
    $('.tab-content:eq('+ currentTabIndex +')').addClass('active-content').parents('.tabs-content').find('.tab-content').not($('.tab-content:eq('+ currentTabIndex +')')).removeClass('active-content');
});

